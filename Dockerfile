FROM centos:latest

MAINTAINER xiaof <549193902@qq.com>

RUN yum -y update && yum clean all
RUN mkdir /var/www/
RUN mkdir /var/www/config
RUN mkdir /var/www/log
RUN mkdir /var/www/wwwroot
RUN mkdir /var/www/config/nginx
RUN mkdir /var/www/log/nginx
RUN yum -y install gcc pcre-devel openssl openssl-devel wget tar
RUN wget http://tengine.taobao.org/download/tengine-2.1.0.tar.gz
RUN tar zxvf tengine-2.1.0.tar.gz
WORKDIR /tengine-2.1.0
RUN cd /tengine-2.1.0
RUN ./configure --user=root --http-log-path=/var/www/log/nginx_accessr.log --error-log-path=/var/www/log/nginx_error.log --conf-path=/var/www/config/nginx/nginx.conf --group=root --prefix=/usr/local/nginx --with-http_ssl_module --without-http_memcached_module --with-http_realip_module
RUN make && make install
WORKDIR /
RUN cd /
RUN rm -rf tengine-*
COPY ./install/run.sh /usr/src/run.sh
RUN chmod 755 /usr/src/run.sh
RUN yum clean all

VOLUME ["/var/www/wwwroot", "/var/www/log/nginx", "/var/www/config/nginx"]

EXPOSE 80

CMD ["/usr/src/run.sh"]